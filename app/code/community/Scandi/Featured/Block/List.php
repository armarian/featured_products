<?php

class Scandi_Featured_Block_List extends Mage_Core_Block_Template
{

    public function getFeaturedProducts() {
        $result = array();

        $products = Mage::getModel('catalog/product')->getCollection();
        $products->addAttributeToSelect('name');
        $products->addAttributeToSelect('url');
        $products->addFieldToFilter('featured', true);

        foreach ($products as $product) {
            $result[] = array(
                'id' => $product->getId(),
                'name' => $product->getName(),
                'url' => $product->getProductUrl(),
            );
        }

        return $result;
    }
}