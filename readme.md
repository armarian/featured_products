Installation:
-------------

Copy contents of the module to your working Magento installation.
To display output of the module in any CMS Block, put this text via Administrative interface:

{{block type="featured/list" name="featured_list" template="featured/list.phtml"}}

There might additionally be a need to enable the module in a list of trusted ones, go:

System -> Permissions -> Blocks, then press "Add New Block" and define "featured/list" inside.

Configuration:
--------------

In order for the module to work properly, products should be assigned a newly created "Featured" attribute.

Catalog -> Manage Products, "Add Product" or select any existing product
and then tick the Featured attribute as True.

Enjoy magentoeing!