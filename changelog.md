1.0.0

Make content block Featured Products available in Magento.
 - additional back-end configuration for category attributes required.
 
1.1.0

Automatic configuration for category attribute present at beta-testing stage.